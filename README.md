
git clone https://nbhat@bitbucket.org/nbhat/flask.login.git


# 1. install Flask extension
pip install FLask
# 2. check the installation by running the example (python hello.py)

# 3. install Flask-SqlAlchemy extension
pip install Flask-SqlAlchemy

# 4. Download & install PostgreSQL database (this may take a while!)
Install PostgreSQL (https://www.enterprisedb.com/downloads/postgres-postgresql-downloads)

# 4. run the application (python app.py)
#   Go to http://localhost:4000/register to add users
#   Go to http://localhost:4000/login to check authentication

# Try out bcrypt (encrypt/decrypt) hashing here: https://bcrypt-generator.com/
